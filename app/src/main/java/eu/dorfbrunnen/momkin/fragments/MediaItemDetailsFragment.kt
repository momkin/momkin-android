package eu.dorfbrunnen.momkin.fragments

import android.annotation.SuppressLint
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import eu.dorfbrunnen.momkin.R
import eu.dorfbrunnen.momkin.adapters.SubtitleRecyclerViewAdapter
import eu.dorfbrunnen.momkin.content.SubtitleItemContent
import eu.dorfbrunnen.momkin.view_models.MediaItemDetailsViewModel
import eu.dorfbrunnen.momkin.view_models.MediaItemDetailsViewModelFactory
import kotlinx.android.synthetic.main.media_item_details_fragment.*
import kotlinx.android.synthetic.main.media_item_details_fragment.view.*
import kotlinx.coroutines.*
import java.net.URLDecoder


class MediaItemDetailsFragment : Fragment() {

    companion object {
        const val SEEK_INTERVAL = 5 * 1000
    }

    lateinit var mediaPlayer: MediaPlayer
    lateinit var subtitleItemContent: SubtitleItemContent

    private val args: MediaItemDetailsFragmentArgs by navArgs()
    private val viewModel: MediaItemDetailsViewModel by viewModels {
        MediaItemDetailsViewModelFactory(
            args,
            context?.contentResolver
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.media_item_details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val filePaths = args.fileUri.split(" ")
        val audioPath = findAudioPath(filePaths)

        viewModel.getSubtitleItemContent()
            .observe(viewLifecycleOwner, Observer<SubtitleItemContent> {
                with(view?.list_subtitles) {
                    this?.layoutManager = LinearLayoutManager(context)
                    this?.adapter = SubtitleRecyclerViewAdapter(it)
                }
                subtitleItemContent = it
            })
        viewModel.getMediaPlayer().observe(viewLifecycleOwner, Observer<MediaPlayer> {
            mediaPlayer = it
            if (!viewModel.isSetup) {
                mediaPlayer.apply {
                    setAudioAttributes(
                        AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .setUsage(AudioAttributes.USAGE_MEDIA)
                            .build()
                    )
                    context?.let { setDataSource(it, Uri.parse(audioPath)) }
                    prepare()
                }
                viewModel.isSetup = true
                playMediaFile()
            } else if (viewModel.isPlaying) {
                button_play.setImageResource(R.drawable.ic_media_pause)
                viewModel.watchMediaPlayerJob = watchMediaPlayerJob()
            }
            button_speed.text = viewModel.currentSpeed
        })

        text_filename.text = URLDecoder.decode(audioPath, "UTF-8")
        button_play.setOnClickListener() {
            playMediaFile()
        }
        button_seek_back.setOnClickListener() {
            seekBack()
        }
        button_seek_forward.setOnClickListener() {
            seekForward()
        }
        button_speed.setOnClickListener() {
            changeSpeed()
        }
    }

    private fun findAudioPath(filePaths: List<String>): String {
        for (i in filePaths.indices) {
            if (filePaths[i].split(".").last() != "srt") {
                return filePaths[i]
            }
        }
        throw Exception("Audio file not found")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            mediaPlayer.reset()
            viewModel.isSetup = false
            viewModel.isPlaying = false
            viewModel.watchMediaPlayerJob.cancel()
            findNavController().navigateUp()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.watchMediaPlayerJob.cancel()
    }

    private fun watchMediaPlayerJob(): Job {
        val completableJob = Job()
        val coroutineScope = CoroutineScope(Dispatchers.Main + completableJob)
        return coroutineScope.launch {
            while (isActive && subtitleItemContent.ITEMS.size > 0) {
                view?.post(Runnable {
                    updateSubtitleHighlightAndScroll(true)
                })
                delay(100)
            }
        }
    }

    private fun findCurrentSubtitleIndex(): Int {
        if (subtitleItemContent.ITEMS.size == 0) {
            return 0
        }
        var currentIndex = 0
        val currentPosition = mediaPlayer.currentPosition
        for (i in subtitleItemContent.ITEMS.indices) {
            if (subtitleItemContent.ITEMS[i].timeStart <= currentPosition) {
                currentIndex = i
            }
        }
        return currentIndex
    }

    private fun findCurrentScrollIndex(currentIndex: Int): Int {
        if (subtitleItemContent.ITEMS.size == 0) {
            return 0
        }
        var currentScrollIndex = currentIndex
        if (subtitleItemContent.ITEMS.size > currentScrollIndex + 1) {
            currentScrollIndex++
        }
        return currentScrollIndex
    }

    private fun updateSubtitleHighlightAndScroll(preview: Boolean) {
        if (subtitleItemContent.ITEMS.size == 0) {
            return
        }
        val currentIndex = findCurrentSubtitleIndex()
        subtitleItemContent.currentId = subtitleItemContent.ITEMS[currentIndex].id
        view?.list_subtitles?.adapter?.notifyDataSetChanged()

        var currentScrollIndex = currentIndex
        if (preview) {
            currentScrollIndex = findCurrentScrollIndex(currentIndex)
        }
        view?.list_subtitles?.layoutManager?.scrollToPosition(currentScrollIndex)
    }

    private fun playMediaFile() {
        if (viewModel.isPlaying) {
            return stopPlayingMediaFile()
        }
        mediaPlayer.start()
        button_play.setImageResource(R.drawable.ic_media_pause)

        viewModel.isPlaying = true
        viewModel.watchMediaPlayerJob = watchMediaPlayerJob()
    }

    private fun stopPlayingMediaFile() {
        mediaPlayer.pause()
        button_play.setImageResource(R.drawable.ic_media_play)

        viewModel.isPlaying = false
        viewModel.watchMediaPlayerJob.cancel()
    }

    private fun seekBack() {
        val currentPosition = mediaPlayer.currentPosition
        val seekPosition = findPreviousSeekPosition(currentPosition)
        mediaPlayer.seekTo(seekPosition)
        updateSubtitleHighlightAndScroll(false)
    }

    private fun seekForward() {
        val currentPosition = mediaPlayer.currentPosition
        val seekPosition = findNextSeekPosition(currentPosition)
        mediaPlayer.seekTo(seekPosition)
        updateSubtitleHighlightAndScroll(false)
    }

    private fun findPreviousSeekPosition(currentPosition: Int): Int {
        if (subtitleItemContent.ITEMS.size == 0) {
            return currentPosition - SEEK_INTERVAL
        }
        val currentIndex = findCurrentSubtitleIndex()
        if (currentIndex == 0) {
            return 0
        }
        return subtitleItemContent.ITEMS[currentIndex - 1].timeStart.toInt()
    }

    private fun findNextSeekPosition(currentPosition: Int): Int {
        if (subtitleItemContent.ITEMS.size == 0) {
            return currentPosition + SEEK_INTERVAL
        }
        val currentIndex = findCurrentSubtitleIndex()
        if (currentIndex == subtitleItemContent.ITEMS.size - 1) {
            return currentPosition + SEEK_INTERVAL
        }
        return subtitleItemContent.ITEMS[currentIndex + 1].timeStart.toInt()
    }

    @SuppressLint("SetTextI18n")
    private fun changeSpeed() {
        val currentSpeed = button_speed.text
        val playbackParams = mediaPlayer.playbackParams
        if (currentSpeed.equals("1")) {
            playbackParams.speed = 0.75F
        }
        if (currentSpeed.equals("0.75")) {
            playbackParams.speed = 0.5F
        }
        if (currentSpeed.equals("0.5")) {
            playbackParams.speed = 1.25F
        }
        if (currentSpeed.equals("1.25")) {
            playbackParams.speed = 1.5F
        }
        if (currentSpeed.equals("1.5")) {
            playbackParams.speed = 1.75F
        }
        if (currentSpeed.equals("1.75")) {
            playbackParams.speed = 2F
        }
        if (currentSpeed.equals("2")) {
            playbackParams.speed = 1F
        }
        button_speed.text = playbackParams.speed.toString()
        if (playbackParams.speed % 1f == 0f) {
            button_speed.text = playbackParams.speed.toInt().toString()
        }
        viewModel.currentSpeed = button_speed.text as String
        mediaPlayer.playbackParams = playbackParams
        if (!viewModel.isPlaying) {
            stopPlayingMediaFile()
        }
    }

}