package eu.dorfbrunnen.momkin.fragments

import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import eu.dorfbrunnen.momkin.R
import kotlinx.android.synthetic.main.media_item_selection.view.*

/**
 * A fragment allowing to select media items.
 */
class MediaItemSelectionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.media_item_selection, container, false)

        view.text_nothing_selected_action.movementMethod = LinkMovementMethod.getInstance()

        view.button_open_language_media_item.setOnClickListener() {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                type = "*/*"
            }

            activity?.startActivityForResult(intent, INTENT_OPEN_LANGUAGE_MEDIA_CODE)
        }
        return view
    }

    companion object {
        const val INTENT_OPEN_LANGUAGE_MEDIA_CODE = 1
    }
}