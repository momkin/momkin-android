package eu.dorfbrunnen.momkin.adapters

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import eu.dorfbrunnen.momkin.R
import eu.dorfbrunnen.momkin.content.SubtitleItemContent

/**
 * [RecyclerView.Adapter] that can display a [MediaItem].
 */
class SubtitleRecyclerViewAdapter(
    private val mediaItemContent: SubtitleItemContent
) : RecyclerView.Adapter<SubtitleRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.subtitle_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mediaItemContent.ITEMS[position]
        holder.contentView.text = item.content
        holder.secondaryContentView.text = item.secondary_content
        if (item.id == mediaItemContent.currentId) {
            holder.subtitleItem.setBackgroundColor(holder.highlightColor)
        } else {
            holder.subtitleItem.setBackgroundColor(Color.TRANSPARENT)
        }
        if (item.secondary_content == "") {
            holder.secondaryContentView.visibility = View.GONE
        } else {
            holder.secondaryContentView.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int = mediaItemContent.ITEMS.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val highlightColor = view.resources.getColor(R.color.colorHighlightSubtitle, null)
        val subtitleItem: LinearLayout = view.findViewById(R.id.subtitle_item)
        val contentView: TextView = view.findViewById(R.id.content)
        val secondaryContentView: TextView = view.findViewById(R.id.secondary_content)

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }
}