package eu.dorfbrunnen.momkin.content

import java.util.ArrayList

class SubtitleItemContent {

    /**
     * An array of subtitle items.
     */
    val ITEMS: MutableList<SubtitleItem> = ArrayList()

    /**
     * The [SubtitleItem.id] of the currently played [SubtitleItem].
     */
    var currentId: Int = 0

    /**
     * A media item representing a piece of content.
     */
    data class SubtitleItem(
        val id: Int,
        var timeStart: Long = 0L,
        var timeEnd: Long = 0L,
        var content: String = "",
        var secondary_content: String = ""
    )
}