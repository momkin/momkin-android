package eu.dorfbrunnen.momkin.view_models

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.media.MediaPlayer
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import eu.dorfbrunnen.momkin.content.SubtitleItemContent
import eu.dorfbrunnen.momkin.fragments.MediaItemDetailsFragmentArgs
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.InputStream
import kotlin.math.pow


class MediaItemDetailsViewModel(
    private val mFilePaths: MediaItemDetailsFragmentArgs,
    private val mContentResolver: ContentResolver?
) : ViewModel() {

    var isSetup = false
    var isPlaying = false
    var currentSpeed = "1"
    lateinit var watchMediaPlayerJob: Job

    private var subtitleItemContentLiveData = MutableLiveData<SubtitleItemContent>()

    fun getSubtitleItemContent(): MutableLiveData<SubtitleItemContent> {
        return subtitleItemContentLiveData
    }

    private var mediaPlayerLiveData = MutableLiveData<MediaPlayer>()

    fun getMediaPlayer(): MutableLiveData<MediaPlayer> {
        return mediaPlayerLiveData
    }

    init {
        viewModelScope.launch {
            mediaPlayerLiveData.value = MediaPlayer()

            subtitleItemContentLiveData.value = SubtitleItemContent()
            val subtitlePaths = loadSubtitlePaths()
            if (subtitlePaths.first != "") {
                setupSubtitleText(subtitlePaths.first, subtitlePaths.second)
            }
        }
    }

    private fun loadSubtitlePaths(): Pair<String, String> {
        val filePaths = mFilePaths.fileUri.split(" ")
        val subtitlePath = findSubtitlePath(filePaths)
        val subtitleTranslationPath = findSubtitlePath(filePaths, subtitlePath)
        return Pair(subtitlePath, subtitleTranslationPath)
    }

    private fun findSubtitlePath(filePaths: List<String>, ignoredFilePath: String = ""): String {
        for (i in filePaths.indices) {
            if (filePaths[i] != ignoredFilePath && filePaths[i].split(".").last() == "srt") {
                return filePaths[i]
            }
        }
        return ""
    }

    private fun setupSubtitleText(subtitlePath: String, subtitleTranslationPath: String) {
        var inputStream = mContentResolver?.openInputStream(Uri.parse(subtitlePath))
        if (inputStream?.available() == 0) {
            return
        }
        val mySubtitleList = parseSubtitleText(inputStream)

        var mySubtitleTranslationList = mutableListOf<SubtitleItemContent.SubtitleItem>()
        if (subtitleTranslationPath != "") {
            inputStream =
                mContentResolver?.openInputStream(Uri.parse(subtitleTranslationPath))
            if (inputStream?.available() != 0) {
                mySubtitleTranslationList = parseSubtitleText(inputStream)
            }
        }

        val subtitleItemContent = SubtitleItemContent()
        for (i in 0 until mySubtitleList.size) {
            val subtitleItem = mySubtitleList[i]
            if (mySubtitleTranslationList.size > i &&
                subtitleItem.id == mySubtitleTranslationList[i].id
            ) {
                subtitleItem.secondary_content = mySubtitleTranslationList[i].content
            }
            subtitleItemContent.ITEMS.add(subtitleItem)
        }
        subtitleItemContentLiveData.value = subtitleItemContent
    }

    @SuppressLint("SimpleDateFormat")
    private fun parseSubtitleText(
        inputStream: InputStream?
    ): MutableList<SubtitleItemContent.SubtitleItem> {
        val subtitleList = mutableListOf<SubtitleItemContent.SubtitleItem>()
        var subtitleId = 0
        var hadSubtitleId = false
        var subtitleItem = SubtitleItemContent.SubtitleItem(0)
        inputStream?.bufferedReader()?.forEachLine {
            if (it.equals(subtitleId.toString())) {
                subtitleItem = SubtitleItemContent.SubtitleItem(it.toInt())
                subtitleId++
                hadSubtitleId = true
                return@forEachLine
            }
            if (hadSubtitleId) {
                val times = it.split(" --> ")
                subtitleItem.timeStart = textTimeToMilliseconds(times[0])
                subtitleItem.timeEnd = textTimeToMilliseconds(times[1])
                hadSubtitleId = false
                return@forEachLine
            }
            if (it.equals("")) {
                subtitleList.add(subtitleItem)
                return@forEachLine
            }
            if (!subtitleItem.content.equals("")) {
                subtitleItem.content += "\n$it"
                return@forEachLine
            }
            subtitleItem.content = it
        }
        return subtitleList
    }

    /**
     * Parses the [SimpleDateFormat] pattern "hh:mm:ss,SSS".
     */
    private fun textTimeToMilliseconds(timeText: String): Long {
        var time: Long = 0
        val timeCoarseArray = timeText.split(":") // [hh; mm; ss,SSS]
        val timeFineArray = timeCoarseArray[2].split(",") // [ss; SSS]
        time += timeFineArray[1].toLong()
        time += timeFineArray[0].toLong() * 10.0.pow(3.0).toLong()
        time += timeCoarseArray[1].toLong() * 10.0.pow(3.0).toLong() * 60
        time += timeCoarseArray[0].toLong() * 10.0.pow(3.0).toLong() * 60 * 60
        return time
    }
}