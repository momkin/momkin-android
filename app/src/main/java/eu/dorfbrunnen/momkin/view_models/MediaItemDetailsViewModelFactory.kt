package eu.dorfbrunnen.momkin.view_models

import android.content.ContentResolver
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import eu.dorfbrunnen.momkin.fragments.MediaItemDetailsFragmentArgs


class MediaItemDetailsViewModelFactory(
    private val mFilePaths: MediaItemDetailsFragmentArgs,
    private val mContentResolver: ContentResolver?
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MediaItemDetailsViewModel(
            mFilePaths,
            mContentResolver
        ) as T
    }
}