package eu.dorfbrunnen.momkin

import android.app.Activity
import android.content.ClipData
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import eu.dorfbrunnen.momkin.fragments.MediaItemSelectionFragment.Companion.INTENT_OPEN_LANGUAGE_MEDIA_CODE
import eu.dorfbrunnen.momkin.fragments.MediaItemSelectionFragmentDirections


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == INTENT_OPEN_LANGUAGE_MEDIA_CODE && resultCode == Activity.RESULT_OK && data != null) {
            handleLanguageMediaIntent(data)
        }
    }

    private fun handleLanguageMediaIntent(data: Intent) {
        val filePaths = mutableListOf<String>()
        val clipData: ClipData? = data.clipData
        if (clipData != null) {
            for (i in 0 until clipData.itemCount) {
                filePaths.add(clipData.getItemAt(i).uri.toString())
            }
        } else {
            filePaths.add(data.data.toString())
        }
        openLanguageMediaFile(filePaths)
    }

    private fun openLanguageMediaFile(filePaths: MutableList<String>) {
        val action =
            MediaItemSelectionFragmentDirections.actionMediaItemSelectionFragmentToMediaItemDetailsFragment(
                filePaths.joinToString(" ")
            )
        findNavController(R.id.nav_host_fragment).navigate(action)
    }
}