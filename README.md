# Momkin Android App

Momkin (from Arabic [ممكن](https://en.wiktionary.org/wiki/%D9%85%D9%85%D9%83%D9%86), "doable")
helps you learn languages by visualizing what's being said. All you need is some audio file that
goes with a subtitle file (supported format: .srt). You can download those files with apps like
[NewPipe](https://newpipe.schabi.org/), but you can also create them yourself with programs like
[Aegisub](http://www.aegisub.org/).

<img width="500" alt="Screenshot of Momkin showing subtitles (Spanish/English) of YouTube video
'Presentaciones en español - Aprender español conversacion' by Tio Spanish"
src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png">
<img width="500" alt="Screenshot of Momkin showing main screen with possibility to select files"
src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png">

## Download Momkin

You can download Momkin from F-Droid, the free and open source Android app store, or directly [as an
.apk file here from Codeberg](https://codeberg.org/momkin/momkin-android/releases).

[<img src="https://codeberg.org/momkin/momkin-android/raw/branch/main/fdroid_badge.png" width="240">](https://f-droid.org/packages/eu.dorfbrunnen.momkin/)

## License

This program is Free Software: You can use, study share and improve it at your will. Specifically
you can redistribute and/or modify it under the terms of the
[GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.html) as published by the
Free Software Foundation, version 3 of the License.

